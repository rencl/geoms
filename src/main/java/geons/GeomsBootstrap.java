/**
 * Сreate RESTful Web Service that works with the following shapes: circles, triangles, rectangles. 
 * It should be possible to extend the service by adding new shapes.
 * In addition, the service should allow to get, add, edit and delete shapes.
 * Create a separate API that returns list of pre-calculated square areas of particular shapes that are specified in the request set.
 * For all requests/responses, please use JSON format.
 */
package geons;

import static spark.Spark.get;
import static spark.Spark.put;
import static spark.Spark.post;
import static spark.Spark.delete;
import static spark.Spark.options;
import static spark.SparkBase.port;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.ModelAndView;
import spark.SparkBase;
import spark.template.freemarker.FreeMarkerEngine;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;

import geons.model.Shape;
import geons.model.Circle;
import geons.model.Rectangle;
import lombok.Data;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Logger;

/**
 * Geoms REST service 
 * Standalone application uses embeded Jetty & Spark
 * use "geoms/run.sh" to start server
 * 
 * @author p.poluda
 *
 */
public class GeomsBootstrap {

	private static final int maxThreads = 8;
	private static final int minThreads = 2;
	private static final int timeOutMillis = 2000;
	private static final Logger logger = Logger.getLogger(GeomsBootstrap.class
			.getCanonicalName());

	private static ConcurrentSkipListMap<Double, Shape> geoms = new ConcurrentSkipListMap<Double, Shape>();

	private static double id = 0;

	public static synchronized double getId() {
		id++;
		return id;
	}

	public static void main(String[] args) {
		Gson gson = new Gson();
		SparkBase.threadPool(maxThreads, minThreads, timeOutMillis);
		port(8080);// def. 4567
		System.out.println("Geoms initialized.");

		get("/get", (req, res) -> {
			res.status(200);
			res.type("application/json");
			Double fid = Double.parseDouble(req.queryParams("id"));
			return geoms.get(fid);
		}, gson::toJson);

		get("/", (req, res) -> {
			res.status(200);
			res.type("application/json");
			return geoms.values();
		}, gson::toJson);

		post("/add", (req, res) -> {
			ObjectMapper mapper = new ObjectMapper();
			Shape fig = mapper.readValue(req.body(), Shape.class);
			fig.setId(getId());
			geoms.put(new Double(fig.getId()), fig);
			res.status(200);
			res.type("application/json");
			return fig;
		}, gson::toJson);

		put("/edit", (req, res) -> {
			ObjectMapper mapper = new ObjectMapper();
			Shape fig = mapper.readValue(req.body(), Shape.class);
			res.status(200);
			res.type("application/json");
			return geoms.get(new Double(fig.getId())).update(fig);
		}, gson::toJson);

		delete("/del", (req, res) -> {
			Double fid = Double.parseDouble(req.queryParams("id"));
			res.status(200);
			res.type("application/json");
			return geoms.remove(new Double(fid));
		}, gson::toJson);

		post("/api",
				(req, res) -> {
					ObjectMapper mapper = new ObjectMapper();
					JavaType type = mapper.getTypeFactory()
							.constructCollectionType(List.class, Shape.class);
					List<? extends Shape> shapes = mapper.readValue(req.body(),
							new TypeReference<List<? extends Shape>>() {});
					res.status(200);
					res.type("application/json");
					shapes.forEach(e -> e.area());
					return shapes;

				}, gson::toJson);// new JsonTransformer());

	}
}
