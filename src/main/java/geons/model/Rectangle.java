package geons.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Specialization for poly.
 * 
 * @author p.poluda
 *
 */
@Data
public class Rectangle extends Shape {

	@JsonProperty("h")
	double h; // height
	@JsonProperty("w")
	double w; // width

	public Rectangle(float id) {
		super(id);

	}

	public Rectangle() {
		super();
		super.type = "rectangle";

	}

	public Rectangle(float id, double h, double w) {
		super(id);
		super.type = "rectangle";
	}

	@Override
	public double area() {
		super.area_mm = h * w;
		return super.area_mm;
	}

	@Override
	public Rectangle update(Shape fig) {
		Rectangle rf = (Rectangle) fig;
		h = rf.h;
		w = rf.w;
		area();
		return this;
	}
}
