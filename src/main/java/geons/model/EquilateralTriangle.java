/**
 * 
 */
package geons.model;

/**
 * Triangle specialization example
 * 
 * @author p.poluda
 *
 */
public class EquilateralTriangle extends Shape implements IShapeMath {

	double angle_a; // radians
	double angle_b;
	double angle_c;

	/**
	 * 
	 */
	public EquilateralTriangle() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 */
	public EquilateralTriangle(float id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see geons.model.IShapeMath#area()
	 */
	@Override
	public double area() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public EquilateralTriangle update(Shape fig) {
		// TODO Auto-generated method stub
		return this;
	}

}
