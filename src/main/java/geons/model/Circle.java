package geons.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * Spec. for rounded shapes
 * 
 * @author p.poluda
 *
 */
@Data
public class Circle extends Shape {

	@JsonProperty("c")
	double c; // circumference
	@JsonProperty("d")
	double d; // diameter
	@JsonProperty("r")
	double r; // radius

	public Circle(float id) {
		super(id);
	}

	public Circle() {
		super();
		super.type = "circle";
	}

	@JsonCreator
	public Circle(@JsonProperty("r") double r) {
		super();
		super.type = "circle";
		this.r = r;
		area();
	}

	@Override
	public double area() {
		super.area_mm = Math.PI * (r * r);
		return super.area_mm;
	}

	@Override
	public Circle update(Shape fig) {
		Circle rf = (Circle) fig;
		r = rf.r;
		c = rf.c;
		d = rf.d;
		area();
		return this;
	}

}
