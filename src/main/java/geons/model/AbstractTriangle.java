package geons.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * @author p.poluda
 *
 */
@Data
public class AbstractTriangle extends Shape {

	@JsonProperty("base")
	int base;
	@JsonProperty("vertical_height")
	int vertical_height;

	public AbstractTriangle(float id) {
		super(id);
	}

	public AbstractTriangle() {
		super();
		super.type = "triangle";
	}

	@Override
	public double area() {
		super.area_mm = Math.sqrt(base * vertical_height);
		return super.area_mm;
	}

	@Override
	public Shape update(Shape fig) {
		AbstractTriangle rf = (AbstractTriangle) fig;
		base = rf.base;
		vertical_height = rf.vertical_height;
		area();
		return this;
	}

}
