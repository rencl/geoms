package geons.model;

/**
 * Common math methods for all shapes
 * 
 * @author p.poluda
 *
 */
public interface IShapeMath {
	double area();
}
