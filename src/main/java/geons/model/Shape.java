package geons.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.Data;

/**
 * Base class for Geoms
 * 
 * @author p.poluda
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = Circle.class, name = "circle"),
		@JsonSubTypes.Type(value = Rectangle.class, name = "rectangle"),
		@JsonSubTypes.Type(value = AbstractTriangle.class, name = "triangle") 
		})
@Data
public abstract class Shape implements IShapeMath {

	@JsonProperty("area_mm")
	double area_mm;
	@JsonProperty("type")
	String type;
	@JsonProperty("id")
	double id;

	public Shape() {
		super();
	}

	public Shape(double id) {
		super();
		this.id = id;
	}

	public abstract Shape update(Shape fig);

}